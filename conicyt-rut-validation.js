angular.module('RunValidator', ['ngMaterial', 'ngMessages', 'ngMask'])
    .controller('ConicytRunValidatorController', function($scope, validateRUN, $stateParams, $window, $mdDialog, $state, $cookies) {
        $scope.showAdvanced = function(ev) {
            $mdDialog.show({
                controller: DialogController,
                template: '<md-dialog aria-label="Ayuda cédula">' +
                '<form ng-cloak>' +
                '<md-toolbar>' +
                '<div class="md-toolbar-tools">' +
                '<h2>Ayuda - Cédula de Identidad</h2>' +
                '<span flex></span>' +
                '<md-button class="md-icon-button" ng-click="cancel()">' +
                '<md-icon aria-label="Close dialog">close</md-icon>' +
                '</md-button>' +
                '</div>' +
                '</md-toolbar>' +
                '<md-dialog-content>' +
                '<div class="md-dialog-content">' +
                '<img src="https://servicios.conicyt.cl/web/sinexbec2/assets/img/ayuda_cedula.png"> ' +
                '</div>' +
                '</md-dialog-content>' +
                '</form>' +
                '</md-dialog>',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            })
        };

        function DialogController($scope, $mdDialog) {
            $scope.hide = function() {
                $mdDialog.hide();
            };

            $scope.cancel = function() {
                $mdDialog.cancel();
            };
        }

        $scope.user = {};
        var Fn = {
            validaFormatoRut : function (rutCompleto) {
                console.log('validaFormatoRut',rutCompleto.replace(/\./g,''))
                if (!/^[0-9]+-[0-9kK]{1}$/.test( rutCompleto ))
                    return false;
                var tmp     = rutCompleto.split('-');
                var digv    = tmp[1];
                var rut     = tmp[0];
                if ( digv == 'K' ) digv = 'k' ;
                return (Fn.dv(rut) == digv );
            },
            dv : function(T){
                var M=0,S=1;
                for(;T;T=Math.floor(T/10))
                    S=(S+T%10*(9-M++%6))%11;
                return S?S-1:'k';
            }
        };
        $scope.formSubmit = function(userForm, ev){
            if(!Fn.validaFormatoRut($scope.user.run.replace(/\./g,''))){
                userForm.run.$error.runInvalid = true;
            } else {
                var serviceUrl = $scope.serviceUrl + $scope.user.run.replace(/\./g,'') + '/seriales/' + $scope.user.serie;
                var token = $scope.serviceToken;
                validateRUN.isValid(token, serviceUrl, ev).then(function(response){
                    if(response.status == 200) {
                        if(response.data.estado == 'VIGENTE'){
                            userForm.$invalid = false;
                            $scope.submitDisabled = true;
                            $scope.$emit($scope.eventName, {run: $scope.user.run.replace(/\./g,''), response: response});
                        } else {
                            $mdDialog.show(
                                $mdDialog.alert()
                                    .parent(angular.element(document.querySelector('body')))
                                    .clickOutsideToClose(true)
                                    .title('Error')
                                    .textContent('El RUN y el número de documento ingresados no son válidos.')
                                    .ariaLabel('Diálogo alerta')
                                    .ok('Aceptar')
                                    .targetEvent(ev)
                            );
                        }
                    } else {
                        $mdDialog.show(
                            $mdDialog.alert()
                                .parent(angular.element(document.querySelector('body')))
                                .clickOutsideToClose(true)
                                .title('Error')
                                .textContent('Ha ocurrido un error. Por favor intenta de nuevo en unos minutos.')
                                .ariaLabel('Diálogo alerta')
                                .ok('Aceptar')
                                .targetEvent(ev)
                        );
                    }
                });
            }
        };
    })
    .directive('conicytRunValidator', function(){
        return {
            restrict: 'E',
            scope: {
                serviceUrl: '=serviceUrl',
                serviceToken: '=serviceToken',
                eventName: '=eventName'
            },
            template: '<div layout-fill ng-controller="ConicytRunValidatorController">'+
'                <div layout="column" layout-align="center center" layout-fill>' +
'                    <div layout-padding>' +
'                        <p class="md-body-1">Por favor ingrese RUN y número de documento. Si tiene dudas pinche <a href="" ng-click="showAdvanced($event)">aquí</a>.</p>' +
'                    </div>' +
'                    <md-content md-whiteframe="4" layout-padding style="width: 30%;">' +
'                        <form name="userForm" novalidate>' +
'                            <div layout-column>' +
'                                <div flex>' +
'                                    <md-input-container md-is-error="userForm.run.$invalid && (userForm.$submitted || userForm.run.$dirty)" class="md-block">' +
'                                        <label>RUN</label>' +
'                                        <input md-no-asterisk name="run" ng-model="user.run" mask="99.999.999-*" required>' +
'                                        <div ng-messages="userForm.run.$error" ng-show="userForm.$submitted || userForm.run.$dirty" role="alert" md-auto-hide="false">' +
'                                            <div ng-message="required">Debe indicar el RUN</div>' +
'                                            <div ng-message="runError">El RUN no existe en los registros. Si tiene dudas contactarse por sistema de consultas. Clic <a href="http://www.conicyt.cl/becas-conicyt/servicios-al-becario/solicitudes-y-consultas/" target="_blank">aquí</a>.</div>' +
'                                            <div ng-message="runInvalid">El RUN no es válido.</div>' +
'                                        </div>' +
'                                    </md-input-container>' +
'                                </div>' +
'                              ' +
'                                <div flex>' +
'                                    <md-input-container md-is-error="userForm.run.$invalid && (userForm.$submitted || userForm.run.$dirty)" class="md-block">' +
'                                        <label>Número de Documento o de serie de su Cédula</label>' +
'                                        <input md-no-asterisk ng-model="user.serie" name="serie" ng-pattern="/^([A-Z]?)\\+?\\d+[^.]$/" required>' +
'                                        <div ng-messages="userForm.serie.$error" ng-show="userForm.$submitted || userForm.serie.$dirty" role="alert" md-auto-hide="false">' +
'                                            <div ng-message="required">Debe ingresar el número de documento o número de serie</div>' +
'                                            <div ng-message="serieInvalid">El número no es válido.</div>' +
'                                            <div ng-message="pattern">No debe ingresar el caracter punto (.).</div>' +
'                                        </div>' +
'                                    </md-input-container>' +
'                                </div>' +
'                            </div>' +
'                            <div layout="row" layout-align="center center">' +
'                                <md-button flex class="md-raised md-primary" type="submit" ng-click="formSubmit(userForm, $event)" ng-disabled="userForm.$invalid || submitDisabled">Validar</md-button>' +
'                            </div>' +
'                        </form>' +
'                    </md-content>' +
'                </div>' +
'            </div>' 
        };
    })
    .factory('validateRUN', function($http, $mdDialog) {
        var dataFactory = {};
        dataFactory.isValid = function(token, url, ev) {
            return $http({
                method: 'GET',
                url: url,
                headers: {
                    Authorization: 'Bearer ' + token
                }
            }).then(function successCallback(response) {
                return response;
            }, function errorCallback(response) {
                console.log(response);
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('body')))
                        .clickOutsideToClose(true)
                        .title('Error')
                        .textContent('Ha ocurrido un error. Por favor intenta de nuevo en unos minutos.')
                        .ariaLabel('Diálogo alerta')
                        .ok('Aceptar')
                        .targetEvent(ev)
                );
            });
        };
        return dataFactory;
    });